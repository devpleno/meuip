import React from 'react';
import { StyleSheet, Image, Text, View, Button } from 'react-native';

import logo from './assets/logo.png'

export default class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      data: ''
    }

    this.exibirIP = this.exibirIP.bind(this)
  }

  async exibirIP() {
    this.setState({ data: 'Localizando...'})

    const ip = await fetch('http://httpbin.org/ip')

    const data = await ip.json()

    this.setState({ data: data.origin})
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <Image source={logo} />
          <Text style={styles.ip}>{this.state.data}</Text>
          <Button title="Descobrir meu IP" onPress={this.exibirIP}></Button>
        </View>

        <View style={styles.footer}>
          <Text style={styles.feitoPor}>
            Feito por mim
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2f2336',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  ip: {
    color: '#ffffff',
    paddingTop: 20,
    paddingBottom: 20
  },
  footer: {
    backgroundColor: 'brown',
    paddingTop: 10,
    paddingBottom: 10
  },
  feitoPor: {
    color: 'blue',
    textAlign: 'center'
  }
});
